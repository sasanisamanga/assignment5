#include<stdio.h>

int cal_attendees(int price);
int cal_revenue(int price);
int cal_expenditure(int price);
int cal_profit(int price);

void display ();

int cal_attendees(int price)
{
    const int a = 120;
    return a - ((price  - 15) / 5 * 20);
}

int cal_revenue(int price)
{
    return (price) * cal_attendees(price);
}
int cal_expenditure(int price)
{
    const int x = 500;
    return x + 3 * cal_attendees(price);
}
int cal_profit(int price)
{
    return cal_revenue(price) - cal_expenditure(price);
}

void display()
{
    int i = 5 , profit;
    for (i=5;i<=50;i+=5)
       {
        profit = cal_profit(i);
        printf("Rs.%d \t\t Rs.%d\n",i,profit);
       }

}
int main ()
{
    printf("Relationship between profit and ticket price\n\n");
    printf("Ticket price \t profit\n ");
    display();
    printf("\nAccording to above  when increasing the ticket price from Rs.5.00 to Rs.25.00  there is a gradual increase in the profit when increasing the ticket price from Rs.25.00  and above the profit is gradually decreasing, In conclusion, suitable ticket price which he can earn the  highest profit is Rs.25.00.\n");
    return 0;
}
